import * as React from "react";
import { createBrowserRouter } from "react-router-dom";
import {TodoList} from  "./components/TodoList"
import AboutPage from "./pages/AboutPage";
import DoneList from "./pages/todo/DoneList"
import Layout from "./pages/Layout";
import TodoDetail from "./pages/todo/TodoDetail"
import NotFoundPage from "./pages/todo/NotFoundPage";

export const router = createBrowserRouter([
  {
    path: "/",
    element: (
        <Layout />
    ),
    children: [
      {
        index: true,
        element: (
            <TodoList />
        ),
      },
      {
        path: "about",
        element: <AboutPage />,
      },
      {
        path: "done",
        element: <DoneList />,
      },
      {
        path: "todo/:id",
        element: <TodoDetail></TodoDetail>
      },
      {
        path: "*",
        element: <NotFoundPage></NotFoundPage>
      }
    ]
  },
]);
