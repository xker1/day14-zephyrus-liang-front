import { useSelector } from "react-redux";
import { TodoGroup } from "../../components/TodoGroup";
import "./DoneList.css"

const DoneList = () => {
    const todoList = useSelector((state) =>
        state.todo.todoList
    );

    const DownList = todoList.filter((todo) => {
            return todo.done;
        })

    return (
        <div className="donePage">
            <h1>DoneList</h1>
            <TodoGroup todoList={DownList} canEdit={ false } className="doneGroup"/>
        </div>
    )
}
export default DoneList;