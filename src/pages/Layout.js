import { Outlet } from "react-router-dom";
import { Link } from "react-router-dom"
import "./Navigator.css"

const Layout = () => {
    return (
        <div className="layout">
            <div className="header">
                <Link className="link" to="/">HOME</Link>
                <Link className="link" to= "/done">DONE</Link>
                <Link className="link" to="/about">ABOUT</Link>
            </div>
            <Outlet />
        </div>
    )
}
export default Layout;