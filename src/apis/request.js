import axios from "axios"
import { message } from "antd";

const request = axios.create({
    // baseURL: "https://64c0c5650d8e251fd11288fa.mockapi.io/"
    baseURL: "http://localhost:8080"
})

request.interceptors.response.use(
    (response) => response,
    (error) => {
        console.log(error.response.data);
        const msg = error.response.data;
        if (msg !== undefined && msg !== null && msg !== "") {
            message.error(msg);
        } else {
            message.error("Error");
        }
        return Promise.reject(error)
    }
)

export default request;