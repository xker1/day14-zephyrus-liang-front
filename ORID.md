O:
	1. After the Code Review in the morning, the teacher specifically answered several questions about the front-end, including the use of useEffect, which I was not familiar with.
	2. Combining the knowledge from the first two weeks, carry out integrated development of the front and back ends.
	3. In the afternoon, we mainly explained cross domain issues, and there were presentations by various groups with the theme of agile development.
R:
	I feel very fulfilling and full of challenges.
I:
	1. After four days of learning, I have gained a certain mastery of front-end technology, especially when facing React related content.
	2. The group presentation was done quite well, but there are still areas for improvement, such as the order of the PPT presentation and the process of presenting examples.
D:
	I will continue to collect the problems encountered during the learning process to improve my programming skills.